# app-blocker

This will be a free, open source android phone blocker without adds. Several years ago I used to use the SPIN filter-chrome app, because it couldn't just be uninstalled whenever I wanted to uninstall it. I had to request a key to be emailed to an accountability partner, then an hour later it would show up and they would have to send it to me. Once the app was unlocked with the key, then I could either uninstall it or change the settings.

This has since been changed because Google's policies won't allow subscription apps to be undeletable. I'm creating this app as a free replacement that
won't be available on the app store, so it will have to be downloaded and installed on a phone.

## Planned Features

*Items at the top have higher priority*

- A password is required to change app settings and uninstall it.
- Online support website to email a new password in the event one is forgotten.
- **Permadumb** - Solidifies the features so that only a factory reset of the phone allows access again.
- whitelist or blacklist both apps and websites
- allow browsers to open any website from a QR code scan, but don't allow navigation via page links.
- app/website daily time limits
- app/website allowed schedule
- support for Iphone

## Development

Please refer to the following links:

https://stackoverflow.com/questions/7540002/how-to-prevent-an-application-from-being-uninstalled

https://developer.android.com/guide/topics/admin/device-admin